## This repo contains the C++ source code for quadrotor UAV trajectory simulation. 
### Author: Han Yu (https://www.linkedin.com/in/han-yu-goirish/)
### Reference

The simulation is implemented based on the modeling and control framework described in [1]-[3]. A simple linear discrete Kalman Filter is implemented
based on algorithm in [4].

[1]: Quadrotor control: modeling, nonlinear control design, and simulation, Francesco Sabatino, Master Thesis, KTH, Stockholm, Sweden, 2015. 

[2]: Multirotor Aerial Vehicles: Modeling, Estimation, and Control of Quadrotor, Robert Mahony, Vijay Kumar, Peter Corke, IEEE Robotics & Automation Magazine, 2012. 

[3]: Small Unmanned Aircraft: Theory and Practice, Textbook by Randal Beard and Timothy W. McLain, 2012.

[4]: An Introduction to the Kalman Filter, (http://www.cs.unc.edu/~welch/media/pdf/kalman_intro.pdf).

