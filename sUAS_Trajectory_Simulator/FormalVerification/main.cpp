//
//  main.cpp
//  FormalVerification
//
//  Created by Han Yu on 7/1/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include<vector>
#include"z3++.h"

using namespace z3;




/**
 Demonstration of how Z3 can be used to prove validity of
 De Morgan's Duality Law: {e not(x and y) <-> (not x) or ( not y) }
 */
void demorgan() {
    std::cout << "de-Morgan example\n";
    
    context c;
    
    expr x = c.bool_const("x");
    expr y = c.bool_const("y");
    expr conjecture = (!(x && y)) == (!x || !y);
    
    solver s(c);
    // adding the negation of the conjecture as a constraint.
    s.add(!conjecture);
    std::cout << s << "\n";
    std::cout << s.to_smt2() << "\n";
    switch (s.check()) {
        case unsat:   std::cout << "de-Morgan is valid\n"; break;
        case sat:     std::cout << "de-Morgan is not valid\n"; break;
        case unknown: std::cout << "unknown\n"; break;
    }
}


int main() {
    
    try {
        demorgan(); std::cout << "\n";
        std::cout << "done\n";
    }
    catch (exception & ex) {
        std::cout << "unexpected error: " << ex << "\n";
    }
    return 0;
}
