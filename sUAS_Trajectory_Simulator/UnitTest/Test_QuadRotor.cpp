//
//  Test_QuadRotor.cpp
//  UnitTest
//
//  Created by Han Yu on 5/19/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include <stdio.h>
#include "QuadRotor.hpp"
#include "catch.hpp"

TEST_CASE("QuadRotor"){
    SECTION("Testing method QuadRotor::trapz2"){
        std::cout<< "Testing method QuadRotor::trapz2...\n";
        ublas::c_vector<double, 2> beginVal;
        beginVal(0) = 0.0;
        beginVal(1) = 1.0;
        ublas::c_vector<double, 2> endVal;
        endVal(0) = 1.0;
        endVal(1) = 1.0;
        double ts = 0.5;
        ublas::c_vector<double, 2> rtn;
        rtn = QuadRotor::trapz2(beginVal, endVal, ts);
        REQUIRE(rtn(0) == (beginVal(0) + endVal(0))*ts/2);
        REQUIRE(rtn(1) == (beginVal(1) + endVal(1))*ts/2);
    }
}
