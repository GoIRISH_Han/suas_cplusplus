//
//  Test_MatrixLib.cpp
//  UnitTest
//
//  Created by Han Yu on 5/28/18.
//  Copyright © 2018 Yu, Han (GE Global Research, US). All rights reserved.
//

#include <stdio.h>
#include "catch.hpp"
#include "MatrixLib.hpp"

TEST_CASE("MatrixLib"){
    SECTION("Testing Method MatrixLib::vector_init"){
        std::cout << "Testing method MatrixLib::vector_init...\n";
        const unsigned int size = 10;
        double * rtn = NULL;
        rtn = MatrixLib<double>::vector_init(size);
        REQUIRE(rtn != NULL);
        delete [] rtn;
        rtn = NULL;
    }
    
    SECTION("Testing Method MatrixLib::scale"){
        std::cout << "Testing method MatrixLib::scale...\n";
        const double f = 2.0;
        double myarray[] = {1.0, 2.0, 3.0};
        const unsigned int size = 3;
        double * y = myarray;
        MatrixLib<double>::scale(size, f, y);
        REQUIRE(myarray[0] == Approx(2.0));
        REQUIRE(myarray[1] == Approx(4.0));
        REQUIRE(myarray[2] == Approx(6.0));
    }
    
    SECTION("Testing Method MatrixLib::fill"){
        std::cout << "Testing method MatrixLib::fill...\n";
        const double f = 0.5;
        const unsigned int size = 5;
        double myarray[size];
        double * y = myarray;
        MatrixLib<double>::fill(size, f, y);
        REQUIRE(myarray[0] == Approx(0.5));
        REQUIRE(myarray[1] == Approx(0.5));
        REQUIRE(myarray[2] == Approx(0.5));
        REQUIRE(myarray[3] == Approx(0.5));
        REQUIRE(myarray[4] == Approx(0.5));
    }
}
