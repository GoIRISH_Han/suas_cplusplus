//
//  MatrixLib.hpp
//  UnitTest
//
//  Created by Han Yu on 5/28/18.
//  Copyright © 2018 Yu, Han (GE Global Research, US). All rights reserved.
//

#ifndef MatrixLib_hpp
#define MatrixLib_hpp
#include <stdio.h>
#include <vector>
#include <math.h>
#include <boost/numeric/ublas/io.hpp>
template <typename T>
class MatrixLib{
private:
public:
    static void fill(const unsigned size, const T f, T *y);
    
    static void scale(const unsigned size, const T f, T * y);
    
    static T * vector_init (const unsigned size);
    
    static T * matrix_init (const unsigned n_row, const unsigned n_col);
    
    static T dot(const unsigned size, const T * x, const T * y);
    
    static T norm2(const unsigned size, const T *x);
};

template <typename T>
T * MatrixLib<T>::vector_init(const unsigned int size){
    T * rtn = new T[size];
    return rtn;
};

template <typename T>
void MatrixLib<T>::scale(const unsigned int size, const T f, T * y){
    for(unsigned i = 0; i < size; i++){
        y[i] = f * y[i];
    }
};

template <typename T>
void MatrixLib<T>::fill(const unsigned int size, const T f, T *y){
    for(unsigned i = 0; i < size; i++){
        y[i] = f;
    }
}

#endif /* MatrixLib_hpp */
