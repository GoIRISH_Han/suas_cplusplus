//
//  KalmanFilter.hpp
//  UnitTest
//
//  Created by Han Yu on 6/9/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#ifndef KalmanFilter_hpp
#define KalmanFilter_hpp

#include <stdio.h>
#include <Eigen/Dense>
class KalmanFilter{
private:
    Eigen::MatrixXd A;
    Eigen::MatrixXd B;
    Eigen::MatrixXd H;
    Eigen::MatrixXd I;
    int m;
    int n;
    
    Eigen::MatrixXd Q;
    Eigen::MatrixXd R;
    Eigen::MatrixXd P_k_pri;
    Eigen::MatrixXd P_k_pst;
    Eigen::MatrixXd K_k;
    Eigen::VectorXd X_k_pri;
    Eigen::VectorXd X_k_pst;
    Eigen::VectorXd u_k;
    
public:
    KalmanFilter(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B, const Eigen::MatrixXd& H, const Eigen::VectorXd& X_0_pst, const Eigen::MatrixXd& P_0_pst, const Eigen::MatrixXd& Q, const Eigen::MatrixXd& R);
    void predict(const Eigen::VectorXd& u);
    void correct(const Eigen::VectorXd& z);
    Eigen::VectorXd get_X_k_pst();
    Eigen::MatrixXd get_P_k_pst(); 
};
#endif /* KalmanFilter_hpp */
