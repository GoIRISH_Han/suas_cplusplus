//
//  KalmanFilter.cpp
//  UnitTest
//
//  Created by Han Yu on 6/9/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include "KalmanFilter.hpp"
using namespace Eigen;

KalmanFilter::KalmanFilter(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B, const Eigen::MatrixXd& H, const Eigen::VectorXd& X_0_pst, const Eigen::MatrixXd& P_0_pst, const Eigen::MatrixXd& Q, const Eigen::MatrixXd& R){
    
    this->m = (int)H.rows();
    this->n = (int)H.cols();
    this->I = Eigen::MatrixXd::Identity(n,n);
    
    this->A = A;
    this->B = B;
    this->H = H;
    this->X_k_pst = X_0_pst;
    this->P_k_pst = P_0_pst;
    this->Q = Q;
    this->R = R;
};

void KalmanFilter::predict(const Eigen::VectorXd& u){
    u_k = u;
    X_k_pri = A * X_k_pst + B * u_k;
    P_k_pri = A * P_k_pst * A.transpose() + Q;
};

void KalmanFilter::correct(const Eigen::VectorXd& z){
    K_k = P_k_pri * H.transpose() * (H *P_k_pri * H.transpose() + R).inverse();
    X_k_pst = X_k_pri + K_k * (z - H * X_k_pri);
    P_k_pst = (I - K_k * H) * P_k_pri;
};

Eigen::VectorXd KalmanFilter::get_X_k_pst(){
    return X_k_pst; 
}

Eigen::MatrixXd KalmanFilter::get_P_k_pst(){
    return P_k_pst;
}
