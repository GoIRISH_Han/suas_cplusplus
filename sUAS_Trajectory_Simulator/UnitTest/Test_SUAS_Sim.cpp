//
//  Test_SUAS_Sim.cpp
//  UnitTest
//
//  Created by Yu, Han on 5/18/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include <stdio.h>
#include "SUAS_Sim.hpp"
#include "catch.hpp"

TEST_CASE("sUAS_Sim"){
    SUAS_Sim sim;
    SECTION("Testing method sUAS_Sim::linespace"){
        std::cout<< "Testing method sUAS_Sim::linespace...\n";
        ublas::vector<double> rtn = SUAS_Sim::linespace(0.0, PI, 10);
        REQUIRE(rtn(3)-rtn(2) == Approx(PI/9));
        REQUIRE(rtn(7)-rtn(6) == Approx(PI/9));
        REQUIRE(rtn(0) == 0.0);
        REQUIRE(rtn(9) == PI); 
    }
    
    SECTION("Testing method sUAS_Sim::gen3DTrajPattern"){
        std::cout<< "Testing method sUAS_Sim::gen3DTrajPattern...\n";
        WayPointList rtn = sim.gen3DTrajPattern(20.0, 15.0);
        REQUIRE(rtn.X(0) == Approx(20.0 * cos(0.0) + 20.0));
        REQUIRE(rtn.Y(0) == Approx(20.0 * sin(0.0) + 20.0));
        REQUIRE(rtn.Z(0) == Approx(0.0));
    }
}
