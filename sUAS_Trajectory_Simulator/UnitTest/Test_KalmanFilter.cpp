//
//  Test_KalmanFilter.cpp
//  UnitTest
//
//  Created by Han Yu on 6/10/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include "KalmanFilter.hpp"
#include "catch.hpp"

TEST_CASE("Kalman_Filter"){
    std::cout<< "Testing Kalman Filter...\n";
    Eigen::MatrixXd A (1,1);
    Eigen::MatrixXd B (1,1);
    Eigen::MatrixXd H (1,1);
    Eigen::VectorXd X_0_pst(1);
    Eigen::MatrixXd P_0_pst(1,1);
    Eigen::MatrixXd Q(1,1);
    Eigen::MatrixXd R(1,1);
    Eigen::VectorXd y(10);
    Eigen::VectorXd z(1);
    Eigen::VectorXd u(1);
    
    A << 1.0;
    B << 0.0;
    H << 1.0;
    X_0_pst << 0.0;
    P_0_pst << 1.0;
    Q << 0.001;
    R << 0.1;
    y << 0.39, 0.50, 0.48, 0.29, 0.25, 0.32, 0.34, 0.48, 0.41, 0.45;
    
    KalmanFilter kf(A, B, H, X_0_pst, P_0_pst, Q, R);
    
    const double t0 = 0.0;
    const double ts = 0.1;
    double tk = 0.0;
    
    for(int i = 0; i < y.size(); i++){
        while(tk < 1){
            u << 0.0;
            kf.predict(u);
            tk = tk + ts;
        }
        z << y[i];
        kf.correct(z);
        tk = 0.0;
        std::cout << "X_k_pst: " << kf.get_X_k_pst() <<"   "
        << "P_k_pst: " << kf.get_P_k_pst() <<"\n";
    }
};
