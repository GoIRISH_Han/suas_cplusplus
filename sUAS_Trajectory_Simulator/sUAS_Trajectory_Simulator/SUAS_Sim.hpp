//
//  SUAS_Sim.hpp
//  sUAS_Trajectory_Simulator
//
//  Created by Yu, Han on 5/17/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#ifndef SUAS_Sim_hpp
#define SUAS_Sim_hpp

#include <stdio.h>
#include <math.h>
#include "QuadRotor.hpp"

namespace ublas = boost::numeric::ublas;

struct Trajectory{
    double h_d;
    double pn_d;
    double pe_d;
    double psi_d;
    double h_vel_d;
    double pn_vel_d;
    double pe_vel_d; 
};

struct WayPointList{
    ublas::vector<double> X;
    ublas::vector<double> Y;
    ublas::vector<double> Z;
};

class SUAS_Sim{
private:
public:
    SUAS_Sim();
    ~SUAS_Sim();
    bool enableFltPlan;
    double span;
    double ts;
    double time;
    Trajectory refTraj;
    WayPointList fltPlan; 
    ublas::vector<double> tspan;
    ublas::vector<double> fltTime;
    WayPointList gen3DTrajPattern(double radius, double height);
    static ublas::vector<double> linespace(double start, double end, int num); 
};
#endif /* SUAS_Sim_hpp */
