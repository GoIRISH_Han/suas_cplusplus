//
//  SUAS_Sim.cpp
//  sUAS_Trajectory_Simulator
//
//  Created by Yu, Han on 5/17/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include "SUAS_Sim.hpp"

SUAS_Sim::SUAS_Sim(){
    enableFltPlan = false;
    span = 5.0;
    ts = 0.001;
    tspan = linespace(0.0, span, span/ts);
    time = 0.0;
    refTraj.h_d = 0.0;
    refTraj.pn_d = 0.0;
    refTraj.pe_d = 0.0;
    refTraj.h_vel_d = 0.0;
    refTraj.pn_vel_d = 0.0;
    refTraj.pe_vel_d = 0.0;
    refTraj.psi_d = 0.0;
};

SUAS_Sim::~SUAS_Sim(){
};

ublas::vector<double> SUAS_Sim::linespace(double start, double end, int num){
    // catch rarely, throw often
    if (num < 2) {
        throw new std::exception();
    }
    int partitions = num - 1;
    ublas::vector<double> pts (num);
    // length of each segment
    double length = (end - start) / partitions;
    // first, not to change
    pts(0) = start;
    for (int i = 1; i < num - 1; i ++) {
        pts(i) = start + i * length;
    }
    // last, not to change
    pts(num-1) = end;
    return pts;
};

WayPointList SUAS_Sim::gen3DTrajPattern(double radius, double height){
    WayPointList trajPat;
    ublas::vector<double> th = linespace(0.0, 2*PI, 50);
    ublas::vector<double> X (th.size());
    ublas::vector<double> Y (th.size());
    ublas::vector<double> Z (th.size());
    for(int i = 0; i < th.size(); ++ i){
        X(i) = radius * cos(th(i)) + radius;
        Y(i) = radius * sin(th(i)) + radius;
        Z(i) = th(i) * height;
    }
    
    trajPat.X = X;
    trajPat.Y = Y;
    trajPat.Z = Z;
    
    return trajPat;
};
