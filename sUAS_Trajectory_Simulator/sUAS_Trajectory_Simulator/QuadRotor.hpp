//
//  QuadRotor.hpp
//  sUAS_trajectory_simulator
//
//  Created by Han Yu on 5/12/18.
//  Copyright © 2018 Han Yu. All rights reserved.
//

#ifndef QuadRotor_hpp
#define QuadRotor_hpp
#define PI 3.14159265
#define g 9.8

#include <stdio.h>
#include <math.h>
#include <vector>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

struct Inertial {
    double Jxx;
    double Jyy;
    double Jzz;
};

struct Drag {
    double Kdx;
    double Kdy;
    double Kdz;
};

struct Performance {
    double h_vel_Max;
    double pn_vel_Max;
    double pe_vel_Max;
    double rho_vel_Max;
    double theta_vel_Max;
    double psi_vel_Max;
};

namespace ublas = boost::numeric::ublas;

class QuadRotor{
private:
public:
    QuadRotor(const double& mass, const Inertial& J, const Drag& drag, const Performance& acPerformance);
    ~QuadRotor();
    void dynamics();
    static ublas::c_vector<double, 2> trapz2(ublas::c_vector<double, 2> beginVal, ublas::c_vector<double, 2> endVal, double ts);
    static ublas::c_vector<double, 12>trapz12(ublas::c_vector<double, 12> beginVal, ublas::c_vector<double, 12> endVal, double ts); 
    static double saturation(double input, double limit);
    ublas::c_vector<double, 3> Att_dot_init;
    ublas::c_vector<double, 3> Omega_b_dot_init;
    ublas::c_vector<double, 3> Vb_dot_init;
    ublas::c_vector<double, 3> Pi_dot_init;
    
    ublas::c_vector<double, 3> Att_init;
    ublas::c_vector<double, 3> Omega_b_init;
    ublas::c_vector<double, 3> Vb_init;
    ublas::c_vector<double, 3> Pi_init;
    
    ublas::c_vector<double, 3> Att_dot;
    ublas::c_vector<double, 3> Omega_b_dot;
    ublas::c_vector<double, 3> Vb_dot;
    ublas::c_vector<double, 3> Pi_dot;
    
    ublas::c_vector<double, 3> Att;
    ublas::c_vector<double, 3> Omega_b;
    ublas::c_vector<double, 3> Vb;
    ublas::c_vector<double, 3> Pi;
    
    double F;
    double torq_rho;
    double torq_theta;
    double torq_psi;
    
    double F_init;
    double torq_rho_init;
    double torq_theta_init;
    double torq_psi_init;
    
    double Mass;
    
    Inertial J;
    Drag Kd;
    Performance acPerformance;
    
};
#endif /* QuadRotor_hpp */

