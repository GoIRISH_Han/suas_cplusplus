//
//  main.cpp
//  sUAS_trajectory_simulator
//
//  Created by Han Yu on 5/12/18.
//  Copyright © 2018 Han Yu. All rights reserved.
//

#include <iostream>
#include "QuadRotor.hpp"
#include "SUAS_Sim.hpp"
#include "InnerLoopControl.hpp"
#include "IOLinearization.hpp"
#include "OuterLoopControl.hpp"

void predictTrajectory(QuadRotor & uav, SUAS_Sim & sim){
    ublas::c_vector<double,3> Pi_dot_prev = uav.Pi_dot;
    ublas::c_vector<double,3> Att_dot_prev = uav.Att_dot;
    ublas::c_vector<double,3> Vb_dot_prev = uav.Vb_dot;
    ublas::c_vector<double,3> Omega_b_dot_prev = uav.Omega_b_dot;
    ublas::c_vector<double,12> State_dot_prev;
    for(int i = 0; i < 3; i++){
        State_dot_prev(i) = Pi_dot_prev(i);
    }
    for(int i = 0; i < 3; i++){
        State_dot_prev(i+3) = Att_dot_prev(i);
    }
    for(int i = 0; i < 3; i++){
        State_dot_prev(i+6) = Vb_dot_prev(i);
    }
    for(int i = 0; i < 3; i++){
        State_dot_prev(i+9) = Omega_b_dot_prev(i);
    }
    uav.dynamics();
    
    ublas::c_vector<double,3> Pi_dot_curr = uav.Pi_dot;
    ublas::c_vector<double,3> Att_dot_curr = uav.Att_dot;
    ublas::c_vector<double,3> Vb_dot_curr = uav.Vb_dot;
    ublas::c_vector<double,3> Omega_b_dot_curr = uav.Omega_b_dot;
    ublas::c_vector<double,12> State_dot_curr;
    
    for(int i = 0; i < 3; i++){
        State_dot_curr(i) = Pi_dot_curr(i);
    }
    for(int i = 0; i < 3; i++){
        State_dot_curr(i+3) = Att_dot_curr(i);
    }
    for(int i = 0; i < 3; i++){
        State_dot_curr(i+6) = Vb_dot_curr(i);
    }
    for(int i = 0; i < 3; i++){
        State_dot_curr(i+9) = Omega_b_dot_curr(i);
    }
    
    ublas::c_vector<double, 12> delta;
    delta = QuadRotor::trapz12(State_dot_prev, State_dot_curr, sim.ts);
    //std::cout<< "delta :" << delta << "\n";
    for(int i = 0; i < 3; i++){
        uav.Pi(i) = uav.Pi(i) + delta(i);
    }
    
    for(int i = 0; i < 3; i++){
        uav.Att(i) = uav.Att(i) + delta(i+3);
    }
    
    for(int i = 0; i < 3; i++){
        uav.Vb(i) = uav.Vb(i) + delta(i+6);
    }
    
    for(int i = 0; i < 3; i++){
        uav.Omega_b(i) = uav.Omega_b(i) + delta(i+9);
    }
    
}

void simTraj(SUAS_Sim & sim, OuterLoopControl & olCtrl, InnerLoopControl & ilCtrl, IOLinearization & ioLinear, QuadRotor & uav){
    static int k;
    if(sim.time == 0)
        k = 0;
    for(int i = 1; i < sim.tspan.size(); ++ i){
        olCtrl.refInputs.pn_d = sim.refTraj.pn_d;
        olCtrl.refInputs.pe_d = sim.refTraj.pe_d;
        olCtrl.refInputs.pn_vel_d = sim.refTraj.pn_vel_d;
        olCtrl.refInputs.pe_vel_d = sim.refTraj.pe_vel_d;
        olCtrl.outerLoopControlUpdate(uav, sim);
        ilCtrl.refInputs.h_d = sim.refTraj.h_d;
        ilCtrl.refInputs.rho_d = olCtrl.ctrInputs.rho_d;
        ilCtrl.refInputs.theta_d = olCtrl.ctrInputs.theta_d;
        ilCtrl.refInputs.psi_d = sim.refTraj.psi_d;
        ilCtrl.refInputs.h_vel_d = sim.refTraj.h_vel_d;
        ilCtrl.refInputs.rho_vel_d = 0.0;
        ilCtrl.refInputs.theta_vel_d = 0.0;
        ilCtrl.refInputs.psi_vel_d = 0.0;
        //std::cout <<"ilCtrl.refInputs.h_d:" <<ilCtrl.refInputs.h_d<< "\n";
        //std::cout <<"ilCtrl.refInputs.rho_d:" <<ilCtrl.refInputs.rho_d<< "\n";
        ilCtrl.InnerLoopControlUpdate(uav);
        
        ioLinear.refInputs.v1 = ilCtrl.ctrInputs.v1;
        ioLinear.refInputs.v2 = ilCtrl.ctrInputs.v2;
        ioLinear.refInputs.v3 = ilCtrl.ctrInputs.v3;
        ioLinear.refInputs.v4 = ilCtrl.ctrInputs.v4;
        //std::cout <<"ioLinear.refInputs.v2:" <<ioLinear.refInputs.v2 << "\n";
        ioLinear.ioLinearizationUpdate(uav);
        
        uav.F = ioLinear.ctrInputs.F;
        uav.torq_rho = ioLinear.ctrInputs.Torq_rho;
        uav.torq_theta = ioLinear.ctrInputs.Torq_theta;
        uav.torq_psi = ioLinear.ctrInputs.Torq_psi;
        //std::cout <<"uav.F:" << uav.F << "\n";
        //std::cout <<"torq_rho:" << uav.torq_rho << "\n";
        //std::cout <<"torq_theta:" << uav.torq_theta << "\n";
        //std::cout <<"torq_psi:" << uav.torq_psi << "\n";
        predictTrajectory(uav, sim);
        sim.time = sim.time + sim.ts;
        //std::cout <<"sim.time:" <<sim.time << "\n";
        //std::cout <<"uav.h:" <<uav.Pi(2)<< "\n";
    }
    k = k + 1;
}


int main(int argc, const char * argv[]) {
    
    double mass;
    mass = 1.5;
    
    Inertial I;
    I.Jxx = 0.05;
    I.Jyy = 0.05;
    I.Jzz = 0.3;
    
    Drag D;
    D.Kdx = 0.1648;
    D.Kdy = 0.31892;
    D.Kdz = 1.1E-6;
    
    Performance Perf;
    Perf.h_vel_Max = 10;
    Perf.pn_vel_Max = 10;
    Perf.pe_vel_Max = 10;
    Perf.rho_vel_Max = 5;
    Perf.theta_vel_Max = 5;
    Perf.psi_vel_Max = 5;
    
    QuadRotor uav = QuadRotor(mass,I,D,Perf);
    std::cout<< "Instantiate a Quadrotor UAV with the following parameters ...\n";
    std::cout<< "Mass[kg]: "<< uav.Mass <<"\n";
    std::cout<< "Inertial[kg*m^2]: \n";
    std::cout<< "   Ixx = "<< uav.J.Jxx <<"\n";
    std::cout<< "   Iyy = "<< uav.J.Jyy <<"\n";
    std::cout<< "   Izz = "<< uav.J.Jzz <<"\n";
    std::cout<< "Translation drag force coefficient [kg/s]: \n";
    std::cout<< "   Kdx = "<< uav.Kd.Kdx <<"\n";
    std::cout<< "   Kdy = "<< uav.Kd.Kdy <<"\n";
    std::cout<< "   Kdz = "<< uav.Kd.Kdz <<"\n";
    std::cout<< "Maximum velocity [m/s]: \n";
    std::cout<< "   h_vel_Max = "<< uav.acPerformance.h_vel_Max <<"\n";
    std::cout<< "   pn_vel_Max = "<< uav.acPerformance.pn_vel_Max <<"\n";
    std::cout<< "   pe_vel_Max = "<< uav.acPerformance.pe_vel_Max <<"\n";
    std::cout<< "   rho_vel_Max = "<< uav.acPerformance.rho_vel_Max <<"\n";
    std::cout<< "   theta_vel_Max = "<< uav.acPerformance.theta_vel_Max <<"\n";
    std::cout<< "   psi_vel_Max = "<< uav.acPerformance.psi_vel_Max <<"\n";
    
    std::cout<< "Prepare the flight plan ...\n";
    SUAS_Sim sim;
    sim.enableFltPlan = true;
    if(sim.enableFltPlan == false){
        sim.refTraj.h_d = 1.0;
        sim.refTraj.pn_d = 1.0;
        sim.refTraj.pe_d = 1.0;
        sim.refTraj.psi_d = 1.0;
        sim.fltTime = sim.tspan;
    }else{
        double radius = 10.0;
        double height = 30.0;
        sim.fltPlan = sim.gen3DTrajPattern(radius, height);
        sim.refTraj.psi_d = 1.0;
        sim.fltTime = SUAS_Sim::linespace(0.0, sim.span * sim.fltPlan.X.size(), sim.span * sim.fltPlan.X.size()/sim.ts);
    }
    
    if(sim.enableFltPlan == false){
        uav.Pi_init(0) = 0.0;
        uav.Pi_init(1) = 0.0;
        uav.Pi_init(2) = 0.0;
    }else{
        uav.Pi_init(0) = sim.fltPlan.X(0);
        uav.Pi_init(1) = sim.fltPlan.Y(0);
        uav.Pi_init(2) = 0.0;
    }
    
    InnerLoopControl ilCtr (uav.acPerformance.h_vel_Max, uav.acPerformance.rho_vel_Max, uav.acPerformance.theta_vel_Max, uav.acPerformance.psi_vel_Max);
    
    IOLinearization ioLinear;
    
    OuterLoopControl olCtrl(uav.acPerformance.pn_vel_Max, uav.acPerformance.pe_vel_Max);
    
    olCtrl.pid.Kp = 0.4;
    olCtrl.pid.Ki = 0.001;
    olCtrl.pid.Kd = 0.8;
    
    if(sim.enableFltPlan){
        for(int i = 0; i < sim.fltPlan.X.size(); ++i){
            sim.refTraj.pn_d = sim.fltPlan.X(i);
            sim.refTraj.pe_d = sim.fltPlan.Y(i);
            sim.refTraj.h_d = sim.fltPlan.Z(i);
            std::cout<<"time :=" << sim.time << " h_ref :=" << sim.refTraj.h_d <<" pn_ref :=" << sim.refTraj.pn_d << " pe_ref :=" << sim.refTraj.pe_d <<"\n";
            simTraj(sim, olCtrl, ilCtr, ioLinear, uav);
            std::cout<<"time :=" << sim.time << " h_sim :=" << uav.Pi(2) <<" pn_sim :=" << uav.Pi(0) << " pe_sim :=" << uav.Pi(1) <<"\n";
        }
    }
    return 0;
}



