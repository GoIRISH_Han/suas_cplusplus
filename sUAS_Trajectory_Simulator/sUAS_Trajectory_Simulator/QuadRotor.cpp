//
//  QuadRotor.cpp
//  sUAS_trajectory_simulator
//
//  Created by Han Yu on 5/12/18.
//  Copyright © 2018 Han Yu. All rights reserved.
//

#include "QuadRotor.hpp"
#include<iostream>

QuadRotor::QuadRotor(const double& M, const Inertial& I, const Drag& D, const Performance& Perf){
    
    Att_dot_init(0) = 0.0;
    Att_dot_init(1) = 0.0;
    Att_dot_init(2) = 0.0;
    Omega_b_dot_init(0) = 0.0;
    Omega_b_dot_init(1) = 0.0;
    Omega_b_dot_init(2) = 0.0;
    Vb_dot_init(0) = 0.0;
    Vb_dot_init(1) = 0.0;
    Vb_dot_init(2) = 0.0;
    Pi_dot_init(0) = 0.0;
    Pi_dot_init(1) = 0.0;
    Pi_dot_init(2) = 0.0;
    
    Att_init(0) = 0.0;
    Att_init(1) = 0.0;
    Att_init(2) = 0.0;
    Omega_b_init(0) = 0.0;
    Omega_b_init(1) = 0.0;
    Omega_b_init(2) = 0.0;
    Vb_init(0) = 0.0;
    Vb_init(1) = 0.0;
    Vb_init(2) = 0.0;
    Pi_init(0) = 0.0;
    Pi_init(1) = 0.0;
    Pi_init(2) = 0.0;
    
    Att_dot(0) = 0.0;
    Att_dot(1) = 0.0;
    Att_dot(2) = 0.0;
    Omega_b_dot(0) = 0.0;
    Omega_b_dot(1) = 0.0;
    Omega_b_dot(2) = 0.0;
    Vb_dot(0) = 0.0;
    Vb_dot(1) = 0.0;
    Vb_dot(2) = 0.0;
    Pi_dot(0) = 0.0;
    Pi_dot(1) = 0.0;
    Pi_dot(2) = 0.0;
    
    Att(0) = 0.0;
    Att(1) = 0.0;
    Att(2) = 0.0;
    Omega_b(0) = 0.0;
    Omega_b(1) = 0.0;
    Omega_b(2) = 0.0;
    Vb(0) = 0.0;
    Vb(1) = 0.0;
    Vb(2) = 0.0;
    Pi(0) = 0.0;
    Pi(1) = 0.0;
    Pi(2) = 0.0;
    
    F = 0.0;
    torq_rho = 0.0;
    torq_theta = 0.0;
    torq_psi = 0.0;
    
    F_init = 0.0;
    torq_rho_init = 0.0;
    torq_theta_init = 0.0;
    torq_psi_init = 0.0;
    
    Mass = M;
    
    J.Jxx = I.Jxx;
    J.Jyy = I.Jyy;
    J.Jzz = I.Jzz;
    
    Kd.Kdx = D.Kdx;
    Kd.Kdy = D.Kdy;
    Kd.Kdz = D.Kdz;
    
    acPerformance.h_vel_Max = Perf.h_vel_Max;
    acPerformance.pn_vel_Max = Perf.pn_vel_Max;
    acPerformance.pe_vel_Max = Perf.pe_vel_Max;
    acPerformance.rho_vel_Max = Perf.rho_vel_Max;
    acPerformance.theta_vel_Max = Perf.theta_vel_Max;
    acPerformance.psi_vel_Max = Perf.psi_vel_Max;
    
    //std::cout << "Instantiate a quadrotor UAV with load " << Mass * g << "[Newton]!\n";
};

QuadRotor::~QuadRotor(){
};

void QuadRotor::dynamics(){
    
    double rho = Att(0);
    double theta = Att(1);
    double psi = Att(2);
    
    double u = Vb(0);
    double v = Vb(1);
    double w = Vb(2);
    
    double p = Omega_b(0);
    double q = Omega_b(1);
    double r = Omega_b(2);
    
    ublas::matrix<double> m_pi(3,3);
    m_pi(0,0) = cos(theta)*cos(psi);
    m_pi(0,1) = sin(rho)*sin(theta)*cos(psi)-cos(rho)*sin(psi);
    m_pi(0,2) = cos(rho)*sin(theta)*cos(psi)+sin(rho)*sin(psi);
    m_pi(1,0) = cos(theta)*sin(psi);
    m_pi(1,1) = sin(rho)*sin(theta)*sin(psi)+cos(rho)*cos(psi);
    m_pi(1,2) = cos(rho)*sin(theta)*sin(psi)-sin(rho)*cos(psi);
    m_pi(2,0) = -sin(theta);
    m_pi(2,1) = sin(rho)*cos(theta);
    m_pi(2,2) = cos(rho)*cos(theta);
    Pi_dot = ublas::prod(m_pi, Vb);
    //std::cout << "m_Pi :" << m_pi <<"\n";
    //std::cout << "Vb :" << Vb <<"\n";
    //std::cout << "Pi_dot :" << Pi_dot <<"\n";
    ublas::vector<double> v1_vb(3);
    v1_vb(0) = r*v - q*w;
    v1_vb(1) = p*w - r*u;
    v1_vb(2) = q*u - p*v;
    ublas::vector<double> v2_vb(3);
    v2_vb(0) = -g*sin(theta);
    v2_vb(1) = g*cos(theta)*sin(rho);
    v2_vb(2) = g*cos(theta)*cos(rho)-F/Mass;
    ublas::vector<double> v3_vb(3);
    v3_vb(0) = Kd.Kdx*u/Mass;
    v3_vb(1) = Kd.Kdy*v/Mass;
    v3_vb(2) = Kd.Kdz*w/Mass;
    Vb_dot = v1_vb + v2_vb - v3_vb;
    ublas::matrix<double> m_att(3,3);
    m_att(0,0) = 1;
    m_att(0,1) = sin(rho)*tan(theta);
    m_att(0,2) = cos(rho)*tan(theta);
    m_att(1,0) = 0;
    m_att(1,1) = cos(rho);
    m_att(1,2) = -sin(rho);
    m_att(2,0) = 0;
    m_att(2,1) = sin(rho)/cos(theta);
    m_att(2,2) = cos(rho)/cos(theta);
    Att_dot = ublas::prod(m_att,Omega_b);
    
    ublas::vector<double> v1_omega_b(3);
    v1_omega_b(0) = (J.Jyy-J.Jzz)/J.Jxx * q * r;
    v1_omega_b(1) = (J.Jzz-J.Jxx)/J.Jyy * p * r;
    v1_omega_b(2) = (J.Jxx-J.Jyy)/J.Jzz * p * q;
    ublas::vector<double> v2_omega_b(3);
    v2_omega_b(0) = torq_rho/J.Jxx;
    v2_omega_b(1) = torq_theta/J.Jyy;
    v2_omega_b(2) = torq_psi/J.Jzz;
    Omega_b_dot = v1_omega_b + v2_omega_b;
};

ublas::c_vector<double, 2> QuadRotor::trapz2(ublas::c_vector<double, 2> beginVal, ublas::c_vector<double, 2> endVal, double ts){
    return (beginVal + endVal)*ts/2; 
};

ublas::c_vector<double, 12> QuadRotor::trapz12(ublas::c_vector<double, 12> beginVal, ublas::c_vector<double, 12> endVal, double ts){
    return (beginVal + endVal)*ts/2;
}

double QuadRotor::saturation(double input, double limit){
    if(input > limit)
        return limit;
    else if (input < -limit)
        return -limit;
    else
        return input;
};
