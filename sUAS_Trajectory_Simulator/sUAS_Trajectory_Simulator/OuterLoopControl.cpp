//
//  OuterLoopControl.cpp
//  sUAS_Trajectory_Simulator
//
//  Created by Yu, Han on 5/16/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include "OuterLoopControl.hpp"

OuterLoopControl::OuterLoopControl(double pn_vel_Max, double pe_vel_Max){
    refInputs.pn_d = 0.0;
    refInputs.pe_d = 0.0;
    refInputs.pn_vel_d = 0.0;
    refInputs.pe_vel_d = 0.0;
    refInputs.pn_accl_d = 0.0;
    refInputs.pe_accl_d = 0.0;
    refInputs.pn_vel_Max = pn_vel_Max;
    refInputs.pe_vel_Max = pe_vel_Max;
    
    pid.Kp = 0.0;
    pid.Ki = 0.0;
    pid.Kd = 0.0;
    
    ctrInputs.theta_d = 0.0;
    ctrInputs.rho_d = 0.0;
    
    result.x_accel_d = 0.0;
    result.y_accel_d = 0.0;

};

OuterLoopControl::~OuterLoopControl(){
    
};

void OuterLoopControl::lateralPositionControl(QuadRotor & uav, SUAS_Sim & sim){
    
    refInputs.pn_d = sim.refTraj.pn_d;
    refInputs.pe_d = sim.refTraj.pe_d;
    refInputs.pn_vel_d = sim.refTraj.pn_vel_d;
    refInputs.pe_vel_d = sim.refTraj.pe_vel_d;
    double tau = sim.ts/2;
    
    if(sim.time == 0){
        posErrPrev(0) = 0.0;
        posErrPrev(1) = 0.0;
        integrator(0) = 0.0;
        integrator(1) = 0.0;
        differentiator(0) = 0.0;
        differentiator(1) = 0.0;
    }
    
    double pn = uav.Pi(0);
    double pe = uav.Pi(1);
    
    ublas::c_vector<double, 2> posErrCurr;
    posErrCurr(0) = refInputs.pn_d - pn;
    posErrCurr(1) = refInputs.pe_d - pe;
    
    integrator = QuadRotor::trapz2(posErrPrev, posErrCurr, sim.ts);
    differentiator = (2*tau - sim.ts)/(2*tau + sim.ts)* differentiator + 2/(2*tau +sim.ts)*(posErrCurr-posErrPrev);
    
    ublas::c_vector<double, 2> U;
    U = pid.Kp * posErrCurr + pid.Ki * integrator + pid.Kd * differentiator;
    result.x_accel_d = QuadRotor::saturation(U(0), 1.0);
    result.y_accel_d = QuadRotor::saturation(U(1), 1.0);
    posErrPrev(0) = posErrCurr(0);
    posErrPrev(1) = posErrCurr(1);
};

void OuterLoopControl::outerLoopControlUpdate(QuadRotor & uav, SUAS_Sim & sim){

    lateralPositionControl(uav, sim);
    
    double psi = uav.Att(2);
    
    ublas::c_vector<double, 2> U;
    
    ublas::matrix<double> m (2,2);
    m(0, 0) = sin(psi);
    m(0, 1) = -cos(psi);
    m(1, 0) = cos(psi);
    m(1, 1) = sin(psi);
    
    ublas::c_vector<double, 2> v;
    v(0) = result.x_accel_d;
    v(1) = result.y_accel_d;
    
    U = -uav.Mass/uav.F*ublas::prod(m, v);
    ctrInputs.rho_d = U(0);
    ctrInputs.theta_d = U(1);
    
};



