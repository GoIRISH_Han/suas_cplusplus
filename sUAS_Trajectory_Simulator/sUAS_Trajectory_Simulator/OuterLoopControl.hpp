//
//  OuterLoopControl.hpp
//  sUAS_Trajectory_Simulator
//
//  Created by Han Yu on 5/15/18.
//  Copyright © 2018 Han Yu. All rights reserved.
//

#ifndef OuterLoopControl_hpp
#define OuterLoopControl_hpp

#include <stdio.h>
#include "QuadRotor.hpp"
#include "SUAS_Sim.hpp"

struct OL_RefInputs{
    double pn_d;
    double pe_d;
    double pn_vel_d;
    double pe_vel_d;
    double pn_accl_d;
    double pe_accl_d; 
    double pn_vel_Max;
    double pe_vel_Max;
};

struct OL_PIDControl{
    double Kp;
    double Ki;
    double Kd;
};

struct OL_ControlInputs{
    double theta_d;
    double rho_d;
};

struct ResultLateralPositionControl{
    double x_accel_d;
    double y_accel_d;
};

class OuterLoopControl{
private:
    ublas::c_vector<double, 2> posErrPrev;
    ublas::c_vector<double, 2> integrator;
    ublas::c_vector<double, 2> differentiator;
    ResultLateralPositionControl result;
    
public:
    OL_RefInputs refInputs;
    OL_PIDControl pid;
    OL_ControlInputs ctrInputs;
    
    OuterLoopControl(double pn_vel_Max, double pe_vel_Max);
    ~OuterLoopControl();
    void lateralPositionControl(QuadRotor & uav, SUAS_Sim & sim);
    void outerLoopControlUpdate(QuadRotor & uav, SUAS_Sim & sim);
};
#endif /* OuterLoopControl_hpp */

