//
//  InnerLoopControl.hpp
//  sUAS_Trajectory_Simulator
//
//  Created by Yu, Han on 5/15/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#ifndef InnerLoopControl_hpp
#define InnerLoopControl_hpp
#include <stdio.h>
#include <math.h>
#include <vector>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "QuadRotor.hpp"

struct IL_RefInputs{
    double h_d;
    double rho_d;
    double theta_d;
    double psi_d;
    double h_vel_d;
    double rho_vel_d;
    double theta_vel_d;
    double psi_vel_d;
    double h_vel_Max;
    double rho_vel_Max;
    double theta_vel_Max;
    double psi_vel_Max;
};

struct IL_ControlInputs{
    double v1;
    double v2;
    double v3;
    double v4;
};

class InnerLoopControl{
private:
    double contrainedMinTimeOptimalControl(double K1, double K2, double X1_max, double X1_ref, double X2_ref, double x1, double x2);
    int sign(double input); 
public:
    IL_RefInputs refInputs;
    IL_ControlInputs ctrInputs;
    
    InnerLoopControl(double h_vel_Max, double rho_vel_Max, double theta_vel_Max, double psi_vel_Max);
    ~InnerLoopControl();
    void InnerLoopControlUpdate(QuadRotor & uav);
};
#endif /* InnerLoopControl_hpp */
