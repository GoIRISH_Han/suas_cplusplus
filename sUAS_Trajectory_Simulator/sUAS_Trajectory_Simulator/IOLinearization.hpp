//
//  IOLinearization.hpp
//  sUAS_Trajectory_Simulator
//
//  Created by Yu, Han on 5/16/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#ifndef IOLinearization_hpp
#define IOLinearization_hpp

#include <stdio.h>
#include <math.h>
#include <vector>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "QuadRotor.hpp"

struct IO_RefInputs{
    double v1;
    double v2;
    double v3;
    double v4;
};

struct IO_ControlInputs{
    double F;
    double Torq_rho;
    double Torq_theta;
    double Torq_psi;
};

class IOLinearization{
private:
public:
    IO_RefInputs refInputs;
    IO_ControlInputs ctrInputs;
    IOLinearization();
    ~IOLinearization();
    void ioLinearizationUpdate(QuadRotor & uav);
};

#endif /* IOLinearization_hpp */

