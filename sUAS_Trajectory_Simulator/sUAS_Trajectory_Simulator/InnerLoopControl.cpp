//
//  InnerLoopControl.cpp
//  sUAS_Trajectory_Simulator
//
//  Created by Yu, Han on 5/15/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include "InnerLoopControl.hpp"

InnerLoopControl::InnerLoopControl(double h_vel_Max, double rho_vel_Max, double theta_vel_Max, double psi_vel_Max){
    refInputs.h_d = 0.0;
    refInputs.rho_d = 0.0;
    refInputs.theta_d = 0.0;
    refInputs.psi_d = 0.0;
    refInputs.h_vel_d = 0.0;
    refInputs.rho_vel_d = 0.0;
    refInputs.theta_vel_d = 0.0;
    refInputs.psi_vel_d = 0.0;
    refInputs.h_vel_Max = h_vel_Max;
    refInputs.rho_vel_Max = rho_vel_Max;
    refInputs.theta_vel_Max = theta_vel_Max;
    refInputs.psi_vel_Max = psi_vel_Max;
    
    ctrInputs.v1 = 0.0;
    ctrInputs.v2 = 0.0;
    ctrInputs.v3 = 0.0; 
};

InnerLoopControl::~InnerLoopControl(){
};
int InnerLoopControl::sign(double input){
    if (input < 0) return -1;
    if (input > 0) return 1;
    return 0;
};

double InnerLoopControl::contrainedMinTimeOptimalControl(double K1, double K2, double X1_max, double X1_ref, double X2_ref, double x1, double x2){
    bool A = (x1 > -X1_max && x1 < X1_max);
    bool B = (x1 >= -X1_max && x1 <= X1_max);
    double C = K2/(2*K1)*sign(X1_ref-x1)*(pow(x1, 2.0)-pow(X1_ref, 2.0))+X2_ref;
    double D = K2/(2*K1)*(pow(x1, 2.0)-pow(X1_ref, 2.0))+X2_ref;
    double U;
    
    if ((A && x2<C) || (B && (x1<X1_ref) && x2 ==D))
        U = 1;
    else if ((x1 ==-X1_max && (x2!=D)) || (x1 == X1_ref && x2==X2_ref) || (x1 == X1_max && (x2 != -D)))
        U = 0;
    else if ((A && x2>C) || (B && (x1>X1_ref) && (x2 == -D)))
        U = -1;
    else
        U = 0;
    return U;
};

void InnerLoopControl::InnerLoopControlUpdate(QuadRotor & uav){
    double K1_h = 1.0;
    double K2_h = 1.0;
    double h_dot_max = refInputs.h_vel_Max;
    double h_dot_ref = refInputs.h_vel_d;
    double h_ref = refInputs.h_d;
    double h_dot = uav.Pi_dot(2);
    double h = uav.Pi(2);
    double h_accel_d = contrainedMinTimeOptimalControl(K1_h, K2_h, h_dot_max, h_dot_ref, h_ref, h_dot, h);
    
    double K1_rho = 1.0;
    double K2_rho = 1.0;
    double rho_dot_max = refInputs.rho_vel_Max;
    double rho_dot_ref = refInputs.rho_vel_d;
    double rho_ref = refInputs.rho_d;
    double rho_dot = uav.Att_dot(0);
    double rho = uav.Att(0);
    double rho_accel_d = contrainedMinTimeOptimalControl(K1_rho, K2_rho, rho_dot_max, rho_dot_ref, rho_ref, rho_dot, rho);
    //std::cout <<"rho_accel_d:" <<rho_accel_d<< "\n";
    double K1_theta = 1.0;
    double K2_theta = 1.0;
    double theta_dot_max = refInputs.theta_vel_Max;
    double theta_dot_ref = refInputs.theta_vel_d;
    double theta_ref = refInputs.theta_d;
    double theta_dot = uav.Att_dot(1);
    double theta = uav.Att(1);
    double theta_accel_d = contrainedMinTimeOptimalControl(K1_theta, K2_theta, theta_dot_max, theta_dot_ref, theta_ref, theta_dot, theta);
    
    double K1_psi = 1.0;
    double K2_psi = 1.0;
    double psi_dot_max = refInputs.psi_vel_Max;
    double psi_dot_ref = refInputs.psi_vel_d;
    double psi_ref = refInputs.psi_d;
    double psi_dot = uav.Att_dot(2);
    double psi = uav.Att(2);
    double psi_accel_d = contrainedMinTimeOptimalControl(K1_psi, K2_psi, psi_dot_max, psi_dot_ref, psi_ref, psi_dot, psi);
    
    ctrInputs.v1 = h_accel_d;
    ctrInputs.v2 = rho_accel_d;
    ctrInputs.v3 = theta_accel_d;
    ctrInputs.v4 = psi_accel_d;
};
