//
//  IOLinearization.cpp
//  sUAS_Trajectory_Simulator
//
//  Created by Yu, Han on 5/16/18.
//  Copyright © 2018 Yu, Han. All rights reserved.
//

#include "IOLinearization.hpp"

namespace ublas = boost::numeric::ublas;

IOLinearization::IOLinearization(){
    refInputs.v1 = 0.0;
    refInputs.v2 = 0.0;
    refInputs.v3 = 0.0;
    refInputs.v4 = 0.0;
    
    ctrInputs.F = 0.0;
    ctrInputs.Torq_rho = 0.0;
    ctrInputs.Torq_theta = 0.0;
    ctrInputs.Torq_psi = 0.0;
};

IOLinearization::~IOLinearization(){
    
};

void IOLinearization::ioLinearizationUpdate(QuadRotor & uav){
    double Jxx = uav.J.Jxx;
    double Jyy = uav.J.Jyy;
    double Jzz = uav.J.Jzz;
    double mass = uav.Mass;
    double rho_dot = uav.Att_dot(0);
    double theta_dot = uav.Att_dot(1);
    double psi_dot = uav.Att_dot(2);
    double rho = uav.Att(0);
    double theta = uav.Att(1);
    
    ublas::c_vector<double, 4> B;
    B(0) = g;
    B(1) = theta_dot*psi_dot*(Jyy-Jxx)/Jxx;
    B(2) = rho_dot*psi_dot*(Jzz-Jxx)/Jyy;
    B(3) = rho_dot*theta_dot*(Jxx-Jyy)/Jzz;
    
    ublas::c_vector<double, 4> v;
    v(0) = refInputs.v1;
    v(1) = refInputs.v2;
    v(2) = refInputs.v3;
    v(3) = refInputs.v4;
    
    ublas::c_matrix<double, 4, 4> Delta_inv;
    Delta_inv(0,0) = -mass/(cos(theta)*cos(rho));
    Delta_inv(0,1) = 0.0;
    Delta_inv(0,2) = 0.0;
    Delta_inv(0,3) = 0.0;
    Delta_inv(1,0) = 0.0;
    Delta_inv(1,1) = Jxx;
    Delta_inv(1,2) = 0.0;
    Delta_inv(1,3) = 0.0;
    Delta_inv(2,0) = 0.0;
    Delta_inv(2,1) = 0.0;
    Delta_inv(2,2) = Jyy;
    Delta_inv(2,3) = 0.0;
    Delta_inv(3,0) = 0.0;
    Delta_inv(3,1) = 0.0;
    Delta_inv(3,2) = 0.0;
    Delta_inv(3,3) = Jzz;
    
    ublas::vector<double> Alpha;
    Alpha = -1*ublas::prod(Delta_inv, B);
    ublas::c_matrix<double, 4, 4> Belta;
    Belta = Delta_inv;
    ublas::vector<double> U;
    U = Alpha + ublas::prod(Belta, v);
    
    ctrInputs.F = U(0);
    ctrInputs.Torq_rho = U(1);
    //std::cout<<"ctrInputs.Torq_rho:" << ctrInputs.Torq_rho <<"\n";
    ctrInputs.Torq_theta = U(2);
    ctrInputs.Torq_psi = U(3);
};
